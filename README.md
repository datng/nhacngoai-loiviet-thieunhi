Around late May 2021 I and my best friend got obsessed with finding songs taught in schools in Vietnam whose melodies were foreign.

Here's what we found, in no particular order:

| Vietnamese version | Origin |
| --- | --- |
| Binh minh len co con chim non | [Montanhes Araneses](https://www.youtube.com/watch?v=OqZQd-ARb98) |
| Dan ga trong san | [Le coq est mort](https://www.youtube.com/watch?v=bIbshj3-1No) |
| Một ngày xanh ta ca hát vang | [Horch was kommt von draußen rein](https://www.youtube.com/watch?v=JbGKgDtLtvg) |
| Lai đây hỡi chú chim nhỏ xinh dễ thương này | [Alouette, gentille alouette](https://www.youtube.com/watch?v=L_hFw_cWg9U) |
| Ở trường cô dạy em thế | [Чему учат в школе](https://www.youtube.com/watch?v=6D3H2XXA8ek) |
| Alibaba | [Los Garcia - Ali Baba](https://www.youtube.com/watch?v=szMMTfeh03w) |
| Trời sáng rồi dậy đi thôi | [Frère Jacque](https://www.youtube.com/watch?v=XrPiWFxmY24) |
| Nụ cười | [улыбки](https://www.youtube.com/watch?v=m0CXrqOq4sI) |
| Hãy để mặt trời luôn chiếu sáng | [Пусть всегда будет солнце](https://www.youtube.com/watch?v=0o-zYl8VRD0). There's also a German derivative: [Immer liebe die Sonne](https://www.youtube.com/watch?v=4efOW4-o6zw) |
| Ca-chiu-sa | [Катюша](https://www.youtube.com/watch?v=AjZrV4wbdnQ) |
| Khát vọng mùa xuân | [Sehnsucht nach dem Frühling](https://www.youtube.com/watch?v=jJNwz_-_0e0) |
| Trông kia đàn gà con lông vàng | [(I don't know the name of the piece, just that it's from Arkadi Filippenko)](https://www.youtube.com/watch?v=hOkQKVxE320) |

There are also some unconfirmed songs - which are stated in the books, but we were not able to find the source:
- [Đừng Đi, Đàng Kia Có Mưa](https://www.youtube.com/watch?v=1qOZDpHImjc) - supposedly from a Czechoslovakian song
